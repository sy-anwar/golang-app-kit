package logger

import (
	"os"

	"github.com/Workiva/go-datastructures/queue"
)

type filepath struct {
	Debug string
	Info  string
	Error string
}

var debugHandler *handler
var infoHandler *handler
var errorHandler *handler

func init() {
	filepath := filepath{
		Error: os.Getenv("ERROR_LOG_PATH"),
		Info:  os.Getenv("INFO_LOG_PATH"),
		Debug: os.Getenv("DEBUG_LOG_PATH"),
	}

	if filepath.Error == "" {
		filepath.Error = "error.log"
	}

	if filepath.Info == "" {
		filepath.Info = "info.log"
	}

	if filepath.Debug == "" {
		filepath.Debug = "debug.log"
	}

	cleanFile(filepath.Debug)
	cleanFile(filepath.Info)
	cleanFile(filepath.Error)

	debugHandler = &handler{
		level:      "debug",
		filepath:   filepath.Debug,
		logCounter: 0,
		tasks:      queue.New(maxLogQueued),
	}

	infoHandler = &handler{
		level:      "info",
		filepath:   filepath.Info,
		logCounter: 0,
		tasks:      queue.New(maxLogQueued),
	}

	errorHandler = &handler{
		level:      "error",
		filepath:   filepath.Error,
		logCounter: 0,
		tasks:      queue.New(maxLogQueued),
	}
}

// Debug function
func Debug(message string) {
	debugHandler.putLog(message)
}

// Info function
func Info(message string) {
	infoHandler.putLog(message)
}

// Error function
func Error(message interface{}) {
	errorHandler.putLog(message)
}
