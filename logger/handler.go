package logger

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/Workiva/go-datastructures/queue"
)

const (
	maxLogCounter = 4096
	maxLogQueued  = 256
)

type handler struct {
	level      string
	filepath   string
	logCounter int16
	tasks      *queue.Queue
	running    bool
	lock       sync.Mutex
}

func (h *handler) putLog(message interface{}) {
	h.lock.Lock()
	defer h.lock.Unlock()

	if !h.running {
		h.running = true
		go h.startLogProcessor()
	}

	if h.tasks.Len() < maxLogQueued {
		h.tasks.Put(message)
	}
}

func (h *handler) startLogProcessor() {
	var task interface{}
	var err error

	for {
		var tasks []interface{}
		task = nil

		h.lock.Lock()
		if h.tasks.Len() != 0 {
			tasks, err = h.tasks.Get(1)
			if err != nil {
				defer h.lock.Unlock()
				break
			}

			task = tasks[0]
		} else {
			defer h.lock.Unlock()
			break
		}
		h.lock.Unlock()
		if h.logCounter == maxLogCounter {
			cleanFile(h.filepath)
			h.logCounter = 0
		}

		h.proceedTask(task)
		h.logCounter++
	}

	if err != nil {
		log.Println(err)
	}

	h.running = false
}

func (h *handler) proceedTask(task interface{}) {
	message := fmt.Sprintf("%s", task)
	message, _ = appendLogLevel(h.level, message)

	writeToFile(h.filepath, message)
	log.Println(message)
}

func appendLogLevel(level, jsonString string) (string, error) {
	jsonMap := make(map[string]interface{})

	err := json.Unmarshal([]byte(jsonString), &jsonMap)
	if err != nil {
		return jsonString, err
	}

	jsonMap["level"] = level

	newJSON, err := json.Marshal(jsonMap)
	if err != nil {
		return jsonString, err
	}

	return string(newJSON), nil
}

func writeToFile(filepath, message string) {
	flag := os.O_APPEND | os.O_CREATE | os.O_WRONLY

	f, err := os.OpenFile(filepath, flag, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	if _, err := f.WriteString(message + "\n"); err != nil {
		log.Println(err)
	}
}

func cleanFile(filepath string) {
	if err := os.Remove("./" + filepath); err != nil {
		if !os.IsNotExist(err) {
			log.Println(err)
		}
	}
}
