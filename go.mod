module gitlab.com/sy-anwar/golang-app-kit

go 1.14

require (
	github.com/Workiva/go-datastructures v1.0.52
	github.com/go-sql-driver/mysql v1.5.0
	github.com/streadway/amqp v1.0.0
	go.mongodb.org/mongo-driver v1.4.4
)
