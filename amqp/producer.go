package amqp

import amqpClient "github.com/streadway/amqp"

// Producer type
type Producer struct {
	amqp
}

// ContentType type
type ContentType string

const (
	// ContentTypePlain const
	ContentTypePlain ContentType = "text/plain"
	// ContentTypeJSON const
	ContentTypeJSON ContentType = "application/json"
)

// CreateProducer create amqp client for receiving message
func CreateProducer(options Options) *Producer {
	producer := &Producer{
		amqp: amqp{
			options: options,
		},
	}

	producer.setConnection()
	producer.setChannel()
	producer.declareExchange()

	return producer
}

// Publish method for publishing message
func (m *Producer) Publish(message []byte, ct ContentType) error {
	err := m.channel.Publish(
		m.options.Exchange.Name,
		m.options.RoutingKey,
		false,
		false,
		amqpClient.Publishing{
			ContentType: string(ct),
			Body:        message,
		})

	return err
}
